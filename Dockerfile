FROM rust:1.35.0-stretch

ENV RUNTIME_USER runtime
ENV RUNTIME_GROUP runtime
ENV RUNTIME_HOME /home/runtime
ENV PROJECT_ROOT /project

COPY ./artifacts /tmp/artifacts

RUN set -xeu \
    && mkdir -p $RUNTIME_HOME \
    && groupadd $RUNTIME_GROUP \
    && useradd --home-dir $RUNTIME_HOME -g $RUNTIME_GROUP $RUNTIME_USER \
    && chown -R $RUNTIME_USER:$RUNTIME_GROUP $RUNTIME_HOME \
    && echo "setting up project mount point" \
    && mkdir $PROJECT_ROOT \
    && echo "You need to mount this directory." > $PROJECT_ROOT/README.md \
    && chown -R $RUNTIME_USER:$RUNTIME_GROUP $PROJECT_ROOT \
    && echo "setting up entrypoint" \
    && cp /tmp/artifacts/docker-entrypoint.sh /docker-entrypoint.sh \
    && chmod 755 /docker-entrypoint.sh

ENTRYPOINT ["/docker-entrypoint.sh"]
WORKDIR $PROJECT_ROOT
VOLUME [$PROJECT_ROOT]

